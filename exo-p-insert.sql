--creer une procedure permettant d'inserer un departement dans la table dept
create or replace procedure pInsert(vCode dept.deptno%type,vNom dept.dname%type,
									vVille dept.loc%type)
is
BEGIN

	insert into dept
		values(vCode,vNom,vVille);

END;
/
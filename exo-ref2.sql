--declaration par reference 
truncate table resultat;
DECLARE
	vEmp emp%rowtype; 
	
BEGIN
	select ename,job,sal
	into vEmp.ename,vEmp.job,vEmp.sal
	from emp 
	where empno = 7902 ;

	insert into resultat 
		values(vEmp.ename,'est un '||vEmp.job||' qui gagne '||vEmp.sal);
END;
/
select * from resultat ;
--creer une procedure permettant d'augmenter le salaire des employes d'un certain montant
create or replace procedure pUpdate(vMont number)
is 
BEGIN

	update employe
		set sal = sal + vMont ;

END;
/
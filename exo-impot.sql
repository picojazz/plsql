--crer un script qui insere dans resultat,
--l'impot qui est de 5% du salaire d'un employe choisi par l'utilisateur
truncate table resultat;
accept mat prompt "matricule de l'employe: ";
DECLARE
	vNom employe.ename%type;
	vImpot employe.sal%type;
BEGIN
	select ename,sal*0.05 into vNom,vImpot
	from employe 
	where empno = &mat ;

	insert into resultat
		values(vNom,'doit payer '||vImpot||' euros d''impots');
END;
/
select * from resultat;

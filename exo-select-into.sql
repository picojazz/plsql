/* la commande SELECT... INTO : recupere des valeurs dans unne table pour le stocker
dans des variables

SELECT col1,col2...
INTO var1,var2...
FROM table
WHERE condition;

exo : creer un script plsql qui permet d'inserer dans la table resultat ,le nom,
la fonction et le salaire de l'employé n°7902
*/

truncate table resultat;
DECLARE
	vNom varchar2(20);
	vFonction varchar2(20);
	vSalaire number;
BEGIN
	select ename,job,sal
	into vNom,vFonction,vSalaire
	from emp 
	where empno = 7902 ;

	insert into resultat 
		values(vNom,'est un '||vFonction||' qui gagne '||vSalaire);
END;
/
select * from resultat ;

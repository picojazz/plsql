-- table de multiplication de 5
truncate table resultat;
DECLARE
	vCpt number := 1 ;
	
BEGIN
	while vCpt < 13
	loop
		insert into resultat
			values(vCpt||' * 5 =',vCpt*5);
		vCpt := vCpt + 1 ;

	end loop;
	

END;
/
select * from resultat ;
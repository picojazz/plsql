--creer un trigger qui insere dans resultat le nom et la date de deconnexion de l'utilisateur
create or replace trigger Tdbconnexion
before logoff on database
BEGIN
	insert into scott.resultat
		values(user,'s''est deconnecté le '||to_char(sysdate,'dd/mm/yyyy,hh24:mi:ss'));
END;
/
truncate table resultat;

create or replace procedure pImpot(mat number)
is

	vNom employe.ename%type;
	vImpot employe.sal%type;
BEGIN
	select ename,sal*0.05 into vNom,vImpot
	from employe 
	where empno = mat ;

	insert into resultat
		values(vNom,'doit payer '||vImpot||' euros d''impots');
END;
/


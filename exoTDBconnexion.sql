--creer un trigger qui insere dans resultat le nom et la date de connexion de l'utilisateur
create or replace trigger Tdbconnexion
after logon on database
BEGIN
	insert into scott.resultat
		values(user,'s''est connecté le '||to_char(sysdate,'dd/mm/yyyy,hh24:mi:ss'));
END;
/
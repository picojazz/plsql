-- creer une fonction fEmploye qui a partir du matricule ramene le nom,
-- l'afficher fonction, le salaire et le numero de departement
create or replace function fEmploye(vMat number)
return varchar2 is
vEmploye varchar2(80);
begin
	select ename||' '||job||' '||sal||' '||deptno
	into vEmploye
	from employe
	where empno=vMat;

	return(vEmploye);
end;
/
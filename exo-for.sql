-- table de multiplication d'un nombre choisi par l'utilisateur
accept n prompt 'nombre a multiplier :';
truncate table resultat;
DECLARE
	vCpt number ;
	
BEGIN
	for vCpt in 1..12
	loop
		insert into resultat
			values(vCpt||' * '||&n||' =',vCpt*&n);
	end loop;
	

END;
/
select * from resultat ;
-- creer un script permettant d'augmenter le salaire des employés managers de 10%
BEGIN
	update employe
		set sal = sal * 1.10
		where job = 'MANAGER';
END;
/
select * from employe;
-- recuperer une valeur 
truncate table resultat;
accept n1 prompt 'Donner le 1er nombre :';
accept n2 prompt 'Donner le 2e nombre :';

Declare

 a number(6) := &n1;
 b number(4) := &n2;
 s number;

Begin

 s:= a + b;

 insert into resultat values(s,'est la somme de '||a||' et '||b);

End;
/
select * from resultat;
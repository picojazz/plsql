--crer un script qui insere dans resultat,
--l'impot qui est de 5% du salaire de tous les employe
truncate table resultat;
DECLARE
	vNom employe.ename%type;
	vImpot employe.sal%type;
	cursor cImpot is select ename,sal*0.05 from employe ;
BEGIN
	open cImpot;
	loop
		fetch cImpot into vNom,VImpot;
	exit when cImpot%notfound;
		
		insert into resultat
		values(vNom,'doit payer '||vImpot||' euros d''impots');
	end loop;
	close cImpot ;
END;
/
select * from resultat;

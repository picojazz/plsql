-- creer un script qui insere dans resultat la somme de n premiers entiers
accept n prompt 'nombre :';
truncate table resultat;
DECLARE
	somme number := 0 ;
	vCpt number ;
	
BEGIN
	for vCpt in 1..&n
	loop
		somme := somme + vCpt ;
	end loop;
	
	insert into resultat (commentaire)
		values('la somme des '||&n||' entiers est de '||somme);
END;
/
select * from resultat ;
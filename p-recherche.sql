-- creer une procedure nommée pRech qui permet de verifier q'une adresse
-- mail saisie par un utilisateur contient le caractere special"@" et
-- l'afficher sinon afficher " vous devez saisir le caractere special "@""
create or replace procedure pRech(vMail varchar2)
is
BEGIN
	if instr(vMail,'@')=0 then
		DBMS_OUTPUT.PUT_LINE('vous devez saisir le caractere special "@"');
	else
		DBMS_OUTPUT.PUT_LINE('votre email est : '||vMail);
	end if;
END;
/
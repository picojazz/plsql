--creer un trigger t_securite permettant d'interdire des mises � jours ou suppression des donn�es dans la table employe
--aux heures et jours non ouvrables

create or replace trigger t_securite
before update or delete on employe
Begin
    if to_char(sysdate,'Day') IN('SAMEDI','DIMANCHE')
    or to_char(sysdate,'hh24:mi') not between '08:00' and '09:00' then
    Raise_application_error(-20005,'Operation interdite');
end if;
End t_securite;
/
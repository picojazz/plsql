--Application1:creer un trigger nomm� t_synchro permettant de synchroniser les tables EMP et EMMPLOYE
--en utilisant comme source la table EMP apres une insertion d'emplyes dans la table EMP

Create or replace trigger t_synchro
After insert on emp
Begin
    MERGE INTO employe d 
    USING emp s
    on (d.empno=s.empno)
    when matched then update 
        set d.ename=s.ename,d.job=s.job,d.mgr=s.mgr,d.hiredate=s.hiredate,d.sal=s.sal,d.comm=s.comm,d.deptno=s.deptno
    when not matched then insert
        values(s.empno,s.ename,s.job,s.mgr,s.hiredate,s.sal,s.comm,s.deptno);
End t_synchro;
/
--creer un script qui permet de synchroniser les table emp et 
--employe en utilisant comme source EMP
BEGIN
	merge into employe d
	using emp e
	on(e.empno = d.empno)
	when matched then update
	set d.ename = e.ename,d.job = e.job,d.mgr = e.mgr,
		d.hiredate = e.hiredate,d.sal = e.sal,d.comm = e.comm,
		d.deptno = e.deptno
	when not matched then insert
		values(e.empno,e.ename,e.job,e.mgr,e.hiredate,e.sal,e.comm,
			   e.deptno);

END;
/
select * from employe;
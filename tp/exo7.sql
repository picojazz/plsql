DECLARE
	vNom employe.ename%type;
	vCom employe.comm%type;
	cursor cEmp is select ename,comm from employe ;
BEGIN
	open cEmp;
	loop
		fetch cEmp into vNom,vCom;
	exit when cEmp%notfound;                                                  
	if vCom is null	then
		insert into resultat
		values(vNom,'ne percoit pas de commission');
	else
		insert into resultat
		values(vNom,vCom);
	end if;
	end loop;
	close cEmp ;
END;
/
select * from resultat;

-- creer un script qui augmente le salaire de l'employe si c'est un manager
--on augment de 500 sinon on diminue de 5%

-- le matricule de l'utilisateur doit etre saisi par l'utilisateur
-- final
Accept mat prompt 'donner le maticule de l''employe: ';
DECLARE
	vFonction employe.job%type;
BEGIN
	select job into vFonction
	from employe
	where empno = &mat ;

	if vFonction = 'MANAGER' then
		update employe
			set sal = sal+500
			where empno = &mat;
	else
		update employe
			set sal = sal*0.95
			where empno = &mat;
	end if;
END;
/
select * from employe;
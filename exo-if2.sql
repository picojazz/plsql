-- creer un script qui insere dans resultat le nom et la commission de l'employé 
-- s'il en a,sinon afficher le nom et "est un cadre"
truncate table resultat;
Accept mat prompt 'donner le maticule de l''employe: ';
DECLARE
	vNom employe.ename%type;
	vCom employe.comm%type;
BEGIN
	select ename,comm into vNom,vCom
	from employe
	where empno = &mat ;

	if vCom is null then
		insert into resultat
			values(vNom,'est un cadre');
	else
		insert into resultat 
			values(vNom,'percoit '||vCOm||' de commission');
	end if;
END;
/
select * from resultat ;
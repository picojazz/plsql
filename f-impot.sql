-- creer une fonction nommée fImpot qui calcule l'impot sur le salaire d'un employe 
--sachant que le taux est de 10%

create or replace function fImpot(vSal number)
return number is
begin
	return(vSal*0.10);
end;
/
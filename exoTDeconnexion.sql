--creer un trigger qui insere dans resultat le nom et la date de deconnexion de l'utilisateur
create or replace trigger Tdeconnexion
before logoff on schema
BEGIN
	insert into resultat
		values(user,'s''est deconnecté le '||to_char(sysdate,'dd/mm/yyyy,hh24:mi:ss'));
END;
/
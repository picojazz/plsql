-- creer une fonction fEmploye qui a partir du matricule ramene le nom,
-- l'afficher fonction, le salaire et le numero de departement
--declarer autant de variable qu'il ya de colonne
create or replace function fEmploye2(vMat number)
return varchar2 is
vEmploye employe%rowtype;
begin
	select ename,job,sal,deptno
	into vEmploye.ename,vEmploye.job,vEmploye.sal,vEmploye.deptno
	from employe
	where empno=vMat;

	return(vEmploye.ename||' '||vEmploye.job||' '||vEmploye.sal||' '||vEmploye.deptno);
end;
/
-- creer un script qui insere dans resultat le nom et la commission de l'employé 
-- et "est un super vendeur" si la commition est superieur a 1000 ou 
-- " est un cadre" si commission est null sinon "percoit ... de commission"
truncate table resultat;
Accept mat prompt 'donner le maticule de l''employe: ';
DECLARE
	vNom employe.ename%type;
	vCom employe.comm%type;
	
BEGIN
	select ename,comm into vNom,vCom
	from employe
	where empno = &mat ;

	

	if vCom is null  then
		insert into resultat
			values(vNom,'est un cadre');
	elsif vCom > 1000 then
		insert into resultat
			values(vNom,'est un super vendeur');
	else
		insert into resultat 
			values(vNom,'percoit '||vCOm||' de commission');
	end if;
END;
/
select * from resultat ;
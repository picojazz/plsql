--creer un script qui insere dans la table resultat le nom,la fonction,le salaire de tous les employés
-- dont le salaire est compris entre 1200 et 3000
truncate table resultat;
DECLARE
	vEmp employe%rowtype;
	
	cursor cEmp is select ename,job,sal from employe where sal between 1200 and 3000 ;
BEGIN
	open cEmp;
	loop
		fetch cEmp into vEmp.ename,vEmp.job,vEmp.sal;
	exit when cEmp%notfound;
		
		insert into resultat (valeur)
		values(vEmp.ename||' '||vEmp.job||' '||vEmp.sal);
	end loop;
	close cEmp ;
END;
/
select * from resultat;

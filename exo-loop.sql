-- creer un script qui affiche les entiers de 0 a 20
truncate table resultat;
DECLARE
	vCpt number := 0 ;
	
BEGIN
	loop
		insert into resultat (commentaire)
			values(vCpt);
		vCpt := vCpt + 1 ;
	exit when vCpt > 20 ;
	end loop;
	

END;
/
select * from resultat ;
--creer un script qui insere dans resltat la somme de 2 nombre
--somme de deux entiers
truncate table resultat;
Declare

 a number(6);
 b number(4);
 s number;

Begin

 a:=125000;
 b:=5300;
 s:= a + b;

 insert into resultat values(s,'est la somme de '||a||' et '||b);

End;
/
select * from resultat;
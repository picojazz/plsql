--creer une procedure nommée pDelete qui permet de supprimer les employes exercant une fontion
-- specifique et ayant un salaire superieur a un motant specifique 
create or replace procedure pDelete(vJob employe.job%type,vSal employe.job%type)
is
BEGIN
	delete employe
		where job = vJob 
		and sal > vSal ;
END;
/
--declaration par reference
truncate table resultat;
DECLARE
	vNom emp.ename%type;
	vFonction emp.job%type;
	vSalaire emp.sal%type;
BEGIN
	select ename,job,sal
	into vNom,vFonction,vSalaire
	from emp 
	where empno = 7902 ;

	insert into resultat 
		values(vNom,'est un '||vFonction||' qui gagne '||vSalaire);
END;
/
select * from resultat ;
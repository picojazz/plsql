--crer un script qui insere dans resultat,
--l'impot qui est de 5% du salaire de n employe saisie par l'utilisateur
-- condition de sortie par while
truncate table resultat;
accept n prompt 'donner le nombre d''employe:';
DECLARE
	vNom employe.ename%type;
	vImpot employe.sal%type;
	cursor cImpot is select ename,sal*0.05 from employe ;
	vCpt number  ;
BEGIN
	open cImpot;
	for vCpt in 1..&n
	loop
		fetch cImpot into vNom,VImpot;
	
		
		insert into resultat
		values(vNom,'doit payer '||vImpot||' euros d''impots');
		
	end loop;
	close cImpot ;
END;
/
select * from resultat;

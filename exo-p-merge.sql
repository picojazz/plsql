--create une procedure nommée p-merge permettant de synchroniser les tables
--emp et employe en utilisant comme source emp
--voir erreur show errors
create or replace procedure pMerge
is
BEGIN
	merge into employe d
	using emp e
	on(e.empno = d.empno)
	when matched then update
	set d.ename = e.ename,d.job = e.job,d.mgr = e.mgr,
		d.hiredate = e.hiredate,d.sal = e.sal,d.comm = e.comm,
		d.deptno = e.deptno
	when not matched then insert
		values(e.empno,e.ename,e.job,e.mgr,e.hiredate,e.sal,e.comm,e.deptno);
END;
/
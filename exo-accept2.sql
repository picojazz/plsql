-- recuperer une valeur 
truncate table resultat;
accept n1 prompt 'Donner le 1er nombre :';
accept n2 prompt 'Donner le 2e nombre :';

Declare

 
 s number;

Begin

 s:= &n1 + &n2;

 insert into resultat values(s,'est la somme de '||&n1||' et '||&n2);

End;
/
select * from resultat;